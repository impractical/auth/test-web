import { Component } from 'preact'

export default class AuthProvider extends Component {

	constructor() {
		super()
		this.state = {
			user: null,
		}
	}

	getChildContext() {
		return {
			user: this.state.user,
			login: this.login,
			logout: this.logout,
		}
	}

	render(props) {
		return props.children[0]
	}

	login = () => {
		console.log('login')
		console.log(this)
		this.setState({user: 'hello world'})
	}

	logout = () => {
		console.log('logout')
		this.setState({user: null})
	}
}
