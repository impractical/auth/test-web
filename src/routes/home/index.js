import { h, Component } from 'preact';
import style from './style';

export default class Home extends Component {
	render (props, state) {
		let message = 'Welcome! Please sign in.'
		if (this.context.user !== null) {
			message = 'Welcome '+this.context.user+'!'
		}
		return (
			<div class={style.home}>
				<h1>Home</h1>
				<p>{message}</p>
			</div>
		)
	}
}
