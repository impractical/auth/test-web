import { Component } from 'preact'
import './style'
import App from './components/app'
import AuthProvider from './providers/auth'

export default class Wrapper extends Component {
	render () {
		return (
			<AuthProvider>
				<App />
			</AuthProvider>
		)
	}
}
